<?php

// Progamation orienté objet =>

    //la class est à mettre dasn un fichier et pour l'appeler ensuite faire un require (comme un include)
    class Voiture{
        public $couleur;
        private $nbRoue;
        public $anneeProduction;
    
        public function __construct($anneeProduction){
            $this->anneeProduction = $anneeProduction;
        }
    
        public function __toString(){
            return $this->couleur . '('.$this->anneeProduction.')';
        }
    
        function definirNbRoue($nbRoue){
            if(round($nbRoue/2) == $nbRoue/2){
                $this->nbRoue = $nbRoue;
            }
        }
    
    }
    
    $voiture1 = new Voiture('2020-01-01');
    $voiture2 = new Voiture('2020-05-06');
    
    $voiture1->couleur = 'vert';
    $voiture2->couleur = 'rouge';
    $voiture1->definirNbRoue(4);
    
    echo "voiture 1 : \n";
     var_dump($voiture1);
     echo "$voiture2 \n";
    //var_dump($voiture2);
    
    
    