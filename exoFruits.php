<?php

class Panier {
    public $fruits =[];

    function ajouterFruit($fruit){
        $this->fruits[] = $fruit;
    }

    function calculerPoids(){
        $p = 0;
        foreach ($this->fruits as $fruit) {
            $p += $fruit->getPoids();
            }
        return $p;
        }
    }


class Fruit{
    public $_variete;
    public $_couleur;
    private $_poids;

    public function __construct($variete, $couleur, $poids){
        $this->_variete = $variete;
        $this->_couleur = $couleur;
        $this->_poids = $poids;
    }

    public function getPoids(){
        return $this->_poids;
    }

}


$panier = new Panier();
$panier->ajouterFruit(new Fruit("pomme", "rouge", 100));
$panier->ajouterFruit(new Fruit("banane", "jaune", 120));

//var_dump($panier);
echo $panier->calculerPoids()."\n"; 