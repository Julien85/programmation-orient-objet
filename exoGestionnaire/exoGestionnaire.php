<?php

require "tache.php";
require "gestionnaire.php";

$tache = new Gestionnaire();
//$tache->ajouterTache(new Tache("test"));

//var_dump($tache);
//echo $tache->listeTache();

do {
    echo "Que souhaitez vous faire? (aide, tache, liste, supprimer, quitter)\n";
    $action = trim(fgets(STDIN));
    if($action == "aide" || $action == "a"){
        echo "Listes des commandes possibles : \n 
    aide/a        afficher les commandes possibles \r
    tache/t       créer une nouvelle tache \r
    liste/l       afficher la liste des tâches en cours \r
    supprimer/s   supprimer une tâche existante \r
    quitter/q     arrêter l'execution du script \r";
    }elseif($action == "tache" || $action == "t"){
        echo "Quel est le nom de votre tache?\n";
        $saisie = trim(fgets(STDIN));
        $tache->ajouterTache(new Tache($saisie));
    }elseif($action == "liste" || $action == "l"){
        echo $tache->listeTache();
    }
}while ($action != "quitter" && $action != "q");

