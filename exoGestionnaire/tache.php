<?php

class Tache {
    private $_tache;
    private $_id = 1;

    public function __construct($tache){
        $this->_tache = $tache;
    }

    public function incre($x){
        $this->_id = $x++;
    }
    
    public function getTache(){
        return $this->_tache;
    }

    public function getId(){
        return $this->_id;
    }
}    

